import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ECharts from 'echarts'
import 'echarts-gl';
Vue.config.productionTip = false
Vue.prototype.$echarts = ECharts;
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
